# ТЗ1 Поиск экстремумов

Подготовлено студентами Финансового университета при Правительстве Российской Федерации по дисциплине Оптимизационные задачи в машинном обучении. Для ознакомления с поставленной задачей нажмите [читать tz1_find_extremums.pdf](illustrations/tz1_find_extremums.pdf)

## Демонстрация возможностей

Множество примеров работы модуля доступны к просмотру любым удобным для Вас способом. <br/>
* Онлайн Google Colab Notebook - нажмите [открыть Google Colab](https://colab.research.google.com/drive/1g7bBh4JvOZbLece2QlwL0XcgxLgY_TW8?usp=sharing)
* Оффлайн Jupyter Notebook - нажмите [скачать tz1_find_extremums.ipynb](tz1_find_extremums.ipynb)

## Иллюстрация работы

* Решение задачи <br/>
![Solution of problem](illustrations/solution_of_problem.png) <br/>
* Вывод задачи <br/>
![Conclusion of problem](illustrations/conclusion_of_problem.png) <br/>
* Интерактивная визуализация <br/>
![Interactive visualization](illustrations/interactive_visualization.gif) <br/>
* Интерактивный ввод <br/>
![Interactive input](illustrations/interactive_input.png) <br/>

## Подготовка окружения

```
# Для визуализации необходим CUDA
!nvidia-smi
!pip install GPUtil
```

## Импорт модуля

```
# Клонирование репозитория
!git clone https://leonidalekseev:maCRxpzWvEcohHKKTeG9@gitlab.com/optimization-tasks-in-machine-learning/tz1_find_extremums.git
# Импорт всех функций из модуля
from tz1_find_extremums.utils import *
```

## Документация функций

`help(set_function)`

```
Help on function set_function in module tz1_find_extremums.utils:

set_function(input_investigated_function: str, functions_symbols: Union[list, NoneType] = None, functions_title: str = 'Введенная \\space функция:', functions_designation: str = 'F', is_display_input: bool = True, _is_system: bool = False) -> tuple
    Установка объекта sympy функции из строки.
    Количество переменных проверяется. Отображение функции настраивается.
    
    Parameters
    ===========
    
    input_investigated_function: str
        Входная строка с функцией
    functions_symbols: list, optional
        Переменные функции
    functions_title: str
        Заголовок для отображения
    functions_designation: str
        Обозначение для отображения
    is_display_input: bool
        Отображать входную функцию или нет
    _is_system: bool
        Вызов функции программой или нет
    
    Returns
    ===========
    
    input_investigated_function: str
        Входная строка с функцией
    investigated_function: sympy
        Исследуемая функция
    functions_symbols: list
        Переменные функции
```

`help(visualize_plotly)`

```
Help on function visualize_plotly in module tz1_find_extremums.utils:

visualize_plotly(input_investigated_function: str, functions_symbols: Union[list, NoneType] = None, points_list: Union[list, NoneType] = None, input_bound_function: Union[str, NoneType] = None, is_display_input: bool = True, is_autosize_graph: bool = True, start: int = -1, stop: int = 1, detail: int = 100, step: Union[float, NoneType] = None) -> None
    Визуализация функции с точками. 
    Границы графика и детализация настроивается.
    
    Parameters
    ===========
    
    input_investigated_function: str
        Исследуемая функция
    functions_symbols: list, optional
        Переменные функции
    points_list: list, optional
        Точки для визуализации
    input_bound_function: str, optional
        Ограничивающая функция
    is_display_input: bool
        Отображать входную функцию или нет
    is_autosize_graph: bool
        Автоматические устанавливать границы или нет
    start: int
        Начало графика
    stop: int
        Конец графика
    detail: int
        Детализация графика
    step: float, optional
        Единичный отрезок графика
```

`help(find_extremums)`

```
Help on function find_extremums in module tz1_find_extremums.utils:

find_extremums(input_investigated_function: str, functions_symbols: Union[list, NoneType] = None, input_bound_function: Union[str, NoneType] = None, bound_symbols: Union[dict, NoneType] = None, is_display_input: bool = True, is_display_solution: bool = True, is_display_conclusion: bool = True, is_approximate_calculations: bool = False, is_try_visualize: bool = True) -> list
    Поиск экстремума функции с заданными параметрами. 
    Ограничивающая функция и ограничения переменных устанавливаются.
    Отображение введенной и ограничивающий функции настраиваются.
    Отображение решения и вывода настраиваются.
    Приближенные вычислениями настраиваются.
    Визуализация графика настраивается.
    
    Parameters
    ===========
    
    input_investigated_function: str
        Входная строка с функцией
    functions_symbols: list, optional
        Переменные функции
    input_bound_function: str, optional
        Ограничивающая функция
    bound_symbols: dict, optional
        Ограничения переменных
    is_display_input: bool = True
        Отображать входную функцию или нет
    is_display_solution: bool = True
        Отображать решение или нет
    is_display_conclusion: bool = True
        Отображать вывод или нет
    is_approximate_calculations: bool = False
        Приближенные вычисления или нет
    is_try_visualize: bool = True
        Визуализация графика функции или нет
    
    Returns
    ===========
    
    points_list: list
        Возвращаемый список точек
```

`help(run_gui)`

```
Help on function run_gui in module tz1_find_extremums.utils:

run_gui(stop: Union[int, NoneType] = None, is_run_function: bool = True) -> dict
    Оболочка для запуска функции find_extremums.
    Опрос по каждому параметру функции find_extremums. 
    Выполните help(find_extremums), чтобы узнать о параметрах.
    
    Parameters
    ===========
    
    stop: int, optional
        Индекс последнего вопроса - параметра функции
    is_run_function: bool
        Запускать функцию с указынными параметрами или нет
    
    Returns
    ===========
    
    parametrs: dict
        Возвращаемый словарь ответов - параметров функции
```

## Участники проекта

* [Быханов Никита](https://gitlab.com/BNik2001) - Менеджер проектa, Тестировщик
* [Алексеев Леонид](https://gitlab.com/LeonidAlekseev) - Программист, Тестировщик
* [Семёнова Полина](https://gitlab.com/polli_eee) - Аналитик, Тестировщик
* [Янина Марина](https://gitlab.com/marinatdd) - Аналитик, Тестировщик
* [Буркина Елизавета](https://gitlab.com/lizaburkina) - Аналитик
* [Егорин Никита](https://gitlab.com/hadesm8) - Аналитик

`Группа ПМ19-1`

## Используемые источники

* [Sympy documentation](https://www.sympy.org/en/index.html)
* [Plotly documentation](https://plotly.com/python/)
